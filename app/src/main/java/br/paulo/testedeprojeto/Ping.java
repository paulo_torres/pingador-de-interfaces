package br.paulo.testedeprojeto;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by nepanuceno on 18/04/15.
 */
public class Ping extends AsyncTask<String, Integer, String> {

    private static final String SU = "su";


    @Override
    protected String doInBackground(String... params) {

        String comando;


        comando = "ping -c "+ params[1] +" -I "+params[2]+ " -i 0.5 "+params[0];
        //comando = "ping -c "+ params[1] +" -i 0.3 "+params[0];

        Log.e("Ping", "Pingando em " + comando);

        try {
            Process process = Runtime.getRuntime().exec(SU);


            DataOutputStream dataProc = new DataOutputStream(process.getOutputStream());
            dataProc.writeBytes("exec " + comando + "\n");
            dataProc.flush();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            BufferedReader error = new BufferedReader(
                    new InputStreamReader(process.getErrorStream()));
            if(error != null) {
                int i;
                char[] buffer = new char[4096];
                StringBuffer output = new StringBuffer();
                while ((i = reader.read(buffer)) > 0)
                    output.append(buffer, 0, i);
                reader.close();
                Log.e("Resultado: ", "Ping via " + params[2] + " - " + output.toString());
                return output.toString() + "\n";
            }
            else {
                Log.e("Error: ", "" + error.readLine());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
