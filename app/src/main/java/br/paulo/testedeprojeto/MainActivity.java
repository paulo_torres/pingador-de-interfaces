package br.paulo.testedeprojeto;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class MainActivity extends ActionBarActivity {

    EditText num_pings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //executaComando("svc wifi enable","wlan0");
        //executaComando("svc data enable","rmnet0");
        //executaComando("svc data prefer","Prioridade da 3G");

        num_pings = (EditText) findViewById(R.id.editText);

    }

    //TODO executa o ping nas interfaces
    public void Ping(View view){
        executaComando("ping -c " + num_pings.getText().toString() + " -I wlan0 8.8.8.8", "wlan0");
        executaComando("ping -c " + num_pings.getText().toString() + " -I rmnet0 8.8.4.4", "rmnet0");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
        @Override
        public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void executaComando(final String comando, final String interf) {
        new Thread() {
            private static final String SU = "su";

            @Override
            public void run() {

                Log.e("Ping", "Pingando em " + comando);

                try {

                    Process process = Runtime.getRuntime().exec(SU);
                    DataOutputStream dataProc = new DataOutputStream(process.getOutputStream());
                    dataProc.writeBytes("exec " + comando + "\n");
                    dataProc.flush();

                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()));

                    BufferedReader error = new BufferedReader(
                            new InputStreamReader(process.getErrorStream()));
                    if (error != null) {
                        int i;
                        char[] buffer = new char[4096];
                        StringBuffer output = new StringBuffer();
                        while ((i = reader.read(buffer)) > 0){
                            output.append(buffer, 0, i);
                            try {

                                Log.i("Line: ", ""+reader.ready()+ " - "+interf);
                                while (reader.ready()) {
                                    String line = reader.readLine().trim();
                                    Log.i("Pingline: ", line);

                                }
                            } catch (IOException e) {

                            }
                        }
                        reader.close();
                        Log.i("Resultado: ", "Ping via " + interf + " - " + output.toString());
                        WriteFile(output.toString(),interf);

                    } else {
                        Log.i("Error: ", "" + error.readLine());

                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    /**
     * Escreve no arquivo texto.
     * @param text Texto a ser escrito.
     * @return True se o texto foi escrito com sucesso.
     */
    public boolean WriteFile(String text, String interf){
        try {
            File file = new File("/sdcard/Pings/pings"+interf+".txt");
            FileOutputStream out = new FileOutputStream(file, true);
            out.write(text.getBytes());
            out.write("\n".getBytes());
            out.flush();
            out.close();
            return true;

        } catch (Exception e) {
            Log.e("Erro: ", e.toString());
            return false;
        }
    }

}